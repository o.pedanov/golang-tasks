package main

import "testing"

func TestAuthomorphic(t *testing.T) {
	var r bool
	r = automorphic(625)
	if r != true {
		t.Error("Expected True, got ", r)
	}

	r = automorphic(62)
	if r != false {
		t.Error("Expected False, got ", r)
	}
}
