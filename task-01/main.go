package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please provide a number")
		return
	}

	if argument, err := strconv.Atoi(os.Args[1]); err == nil {
		if argument < 0 {
			fmt.Println("Number should be positive")
			return
		}
		if automorphic(argument) {
			fmt.Printf("Provided number %d is automorphic\n", argument)
		} else {
			fmt.Printf("Provided number %d is NOT automorphic\n", argument)
		}
	} else {
		fmt.Printf("Provided data %q is NOT a number\n", os.Args[1])
	}
}

// Check if number is automorphic
func automorphic(number int) bool {
	square := number * number
	for i := number; i > 0; {
		if i%10 != square%10 {
			return false
		}
		i /= 10
		square /= 10
	}
	return true
}
