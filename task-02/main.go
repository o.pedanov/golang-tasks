package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please provide a string")
		return
	}
	// Check if provided string is compressed or not for futher action
	if isCompressed(os.Args[1]) {
		fmt.Printf("Uncompressed result:\n%q\n", decompress(os.Args[1]))
	} else {
		fmt.Printf("Compressed result:\n%q\n", compress(os.Args[1]))
	}

}

// Returns True if string has signs of compression
func isCompressed(s string) bool {
	compressed, err := regexp.Match("#[0-9]+#", []byte(s))
	if err != nil {
		log.Fatal("Can't resolve provided string")
	}
	return compressed
}

// Receives string and returns compressed string
func compress(s string) string {
	source := []byte(s)
	var (
		destination string
		index       int
		buffer      []byte
	)
	for i := range source {

		if i == 0 {
			buffer = append(buffer, source[i])
			index++
		} else {
			if source[i] != source[i-1] || i == len(source)-1 {
				switch {
				case index > 4:
					destination += encode(i == len(source)-1, source[i-1], index, source[i])
					buffer = nil
					buffer = append(buffer, source[i])
					index = 1
				case index == 4:
					if i == len(source)-1 {
						destination += encode(i == len(source)-1, source[i-1], index, source[i])
					} else {
						flushBuffer(i == len(source)-1, &buffer, source[i], &destination)
					}
					buffer = nil
					buffer = append(buffer, source[i])
					index = 1
				default:
					flushBuffer(i == len(source)-1, &buffer, source[i], &destination)
					buffer = nil
					buffer = append(buffer, source[i])
					index = 1
				}
			} else {
				buffer = append(buffer, source[i])
				index++
			}
		}
	}
	s = string(destination)
	return s
}

// Receives compressed string and returns uncompressed string
func decompress(s string) string {
	var (
		destination string
		buffer      []byte
	)
	source := []byte(s)
	for i := range source {
		switch {
		case buffer == nil && source[i] == "#"[0]:
			// start buffering
			buffer = append(buffer, source[i])
		case buffer != nil && source[i] != "#"[0]:
			// continue buffering
			buffer = append(buffer, source[i])
			// check if buffered combination is a compression
			// and flush it if no digits found after #
			if !validateCompression(buffer) {
				destination += string(buffer)
				buffer = nil
			}
		case buffer != nil && source[i] == "#"[0]:
			// stop buffering and decode
			buffer = append(buffer, source[i])
			// decode combination if not the end of the string
			if len(source) > i+1 {
				destination += decode(buffer, source[i+1])
				buffer = nil
			}
		default:
			destination += string(source[i])
		}
	}
	// add if something left buffered but not a combination itself
	destination += string(buffer)
	return destination
}

// Validate combination
func validateCompression(buffer []byte) bool {
	combination, err := regexp.Match("#[0-9]+", buffer)
	if err != nil {
		log.Fatal("Error validating combination")
	}
	return combination
}

// Encode a quantity of characters into string
func encode(last bool, char byte, qty int, lastchar byte) string {
	if last && lastchar != char {
		return "#" + strconv.Itoa(qty) + "#" + string(char) + string(lastchar)
	} else if last && lastchar == char {
		return "#" + strconv.Itoa(qty+1) + "#" + string(char)
	} else {
		return "#" + strconv.Itoa(qty) + "#" + string(char)
	}

}

// Decode a quantity of characters into string
func decode(buffer []byte, char byte) string {
	var (
		number []byte
		qty    int
		result string
	)
	// check if combination is not empty and/or has digits
	encoded, err := regexp.Match("#[0-9]+#", buffer)
	if err != nil {
		log.Fatal("Can't resolve provided combination")
	}
	if !encoded {
		return string(buffer)
	}
	for i := range buffer {
		if buffer[i] != "#"[0] {
			number = append(number, buffer[i])
		}
	}
	if qty, err = strconv.Atoi(string(number)); err != nil {
		log.Fatal("Encoded combination conversion error")
	}
	for i := 1; i < qty; i++ {
		result += string(char)
	}
	return result
}

// Receives:
// if the symbol is last in the string as bool,
// pointer to the buffer slice
// last character in the string
// pointer to the destination string
// and flushes the buffer to destination and adds last char if it is the end of the string
func flushBuffer(last bool, buffer *[]byte, char byte, destination *string) {
	*destination += string(*buffer)
	if last {
		*destination += string(char)
	}
}
