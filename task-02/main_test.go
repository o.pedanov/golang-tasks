package main

import "testing"

func TestAuthomorphic(t *testing.T) {
	var r string
	r = compress("kkkkoooooookaaaaaareeeeekoooo#ee3#")
	if r != "kkkk#7#ok#6#ar#5#ekoooo#ee3#" {
		t.Error("Expected \"kkkk#7#ok#6#ar#5#ekoooo#ee3#\", got ", r)
	}

	r = decompress("kkkk#7#ok#6#ar#5#ekoooo#ee3#")
	if r != "kkkkoooooookaaaaaareeeeekoooo#ee3#" {
		t.Error("Expected \"kkkkoooooookaaaaaareeeeekoooo#ee3#\", got ", r)
	}
}
