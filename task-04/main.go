package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"unicode"
)

const maxOffset = 26

func main() {
	// Open text file
	textFile, err := ioutil.ReadFile("./text")
	checkError(err)

	keysFile, err := ioutil.ReadFile("./keys")
	checkError(err)

	// Convert string to slice of words
	separator := func(c rune) bool {
		return !unicode.IsLetter(c) && !unicode.IsNumber(c)
	}
	wordList := strings.FieldsFunc(string(textFile), separator)
	keysList := strings.FieldsFunc(string(keysFile), separator)

	// Find offset if any
	if offset, found := findOffset(wordList, keysList); found != true {
		fmt.Println("Could not decipher provided text")
	} else {
		fmt.Printf("Decrypted text: %s", cipher(wordList, 0-offset))
	}
}

// Receives slice of words to decode and an offset
// Returns decoded string of words
func cipher(text []string, offset int) []string {
	var encodedText []string
	for _, word := range text {
		encodedWord := strings.Map(func(r rune) rune {
			return caesar(r, offset)
		}, word)
		encodedText = append(encodedText, encodedWord)
	}
	return encodedText
}

// Receives slice of encrypted words and slice of keys
// Returns offset in int and true if result found
func findOffset(text []string, keys []string) (int, bool) {
	for offset := -maxOffset; offset <= maxOffset; offset++ {
		encryptedKeys := cipher(keys, offset)
		keysMap := map[string]int{}
		for _, word := range encryptedKeys {
			keysMap[word] = 0
		}
		var counter int
		for _, word := range text {
			if _, ok := keysMap[word]; ok {
				counter++
			}
			if counter == len(keys) {
				return offset, true
			}
		}

	}

	return 0, false
}

// Checks for error and bail out if any
func checkError(e error) {
	if e != nil {
		log.Fatal("Error:", e)
	}
}

func caesar(r rune, shift int) rune {
	al := 26 // Alphabet length
	c := int(r)
	if shift > 0 {
		if c >= 'a' && c <= 'z'-shift ||
			c >= 'A' && c <= 'Z'-shift {
			c = c + shift
		} else if c > 'z'-shift && c <= 'z' ||
			c > 'Z'-shift && c <= 'Z' {
			c = c + shift - al
		}
	} else if shift < 0 {
		shift = 0 - shift
		if c >= 'a'+shift && c <= 'z' ||
			c >= 'A'+shift && c <= 'Z' {
			c = c - shift
		} else if c >= 'a' && c < 'a'+shift ||
			c >= 'A' && c < 'A'+shift {
			c = c - shift + al
		}
	}
	return rune(c)
}
