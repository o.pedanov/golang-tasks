package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strings"
	"unicode"
)

var (
	wordList []string
)

func main() {
	// Read file
	f, err := ioutil.ReadFile("./text")
	if err != nil {
		log.Fatal("Error opening file")
	}

	// Convert text into slice of words
	separate := func(c rune) bool {
		return !unicode.IsLetter(c) && !unicode.IsNumber(c)
	}
	wordList = strings.FieldsFunc(string(f), separate)

	// Range words and add them to a map of words with counters
	words := map[string]int{}

	for _, word := range wordList {
		words[word]++
	}

	// Sort words map
	sortedWords := make([]string, 0, len(words))
	for word := range words {
		sortedWords = append(sortedWords, word)
	}

	sort.Slice(sortedWords, func(i, j int) bool {
		return words[sortedWords[i]] > words[sortedWords[j]]
	})

	// Printout the result
	for _, word := range sortedWords {
		fmt.Printf("%v(%d) ", word, words[word])
	}
	fmt.Println("")

}
